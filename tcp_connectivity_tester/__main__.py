#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

import socketserver

class TCPConnectivityTesterHandler(socketserver.BaseRequestHandler):
    def handle(self):
        self.data = self.request.recv(256).strip()
        print('received: %x'.format(self.data))
        self.request.sendall(self.data[::-1])

if __name__ == "__main__":
    try:
        with socketserver.TCPServer(('0.0.0.0', 23432), TCPConnectivityTesterHandler) as server:
            print('listening on 0.0.0.0:23432')
            server.serve_forever()
    except KeyboardInterrupt:
        pass
